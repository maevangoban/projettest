package com.example.demo.controleurs;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class pagesControler {

	@GetMapping("/")
	public String home(@RequestParam(required=false , defaultValue="word") String name, ModelMap modelMap) {
		//String name = request.getParameter("name") !=null && !request.getParameter("name").isEmpty() ? request.getParameter("name") : "waouhhh";
		modelMap.put("name",name);
		return  "pages/home";
	}
}
